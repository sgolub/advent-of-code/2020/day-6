package main

import (
	"fmt"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "answers"

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 6
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) [][]string {
	result := [][]string{}
	data, ok := days.Control().LoadData(d, useSampleData)[dataKey]
	if !ok {
		return result
	}
	for _, item := range data.([]interface{}) {
		a := []string{}
		for _, s := range item.([]interface{}) {
			a = append(a, s.(string))
		}
		result = append(result, a)
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D6P1")
	count := 0
	for _, cdg := range data {
		m := map[rune]bool{}
		for _, cd := range cdg {
			for _, c := range cd {
				m[c] = true
			}
		}
		count += len(m)
	}
	return fmt.Sprint(count)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D6P2")
	count := 0
	for _, cdg := range data {
		m := map[rune]int{}
		for _, cd := range cdg {
			for _, c := range cd {
				m[c]++
			}
		}
		mapLen := len(cdg)
		for _, i := range m {
			if i == mapLen {
				count++
			}
		}
	}
	return fmt.Sprint(count)
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
